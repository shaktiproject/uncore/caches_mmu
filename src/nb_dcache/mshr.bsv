/* 
see LICENSE.iitm

Author: Arjun Menon, Nitya Ranganathan
Email id: c.arjunmenon@gmail.com, nitya.ranganathan@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package mshr;
  import nb_dcache_types::*;
  import DefaultValue :: *;
  `include "Logger.bsv"
  import FIFO::*;
  import FIFOF::*;
  import ConfigReg::*;
  import Vector::*;
  import SEMF_FIFO::*;
  import SESFMI_FIFO::*;
        import Assert  :: * ;
  `include "nb_dcache.defines"

  interface Ifc_mshr#(numeric type paddr,
                      numeric type linewidthbits,
                      numeric type data,
                      numeric type mshrsize,
                      numeric type mshrfifo_depth,
                      numeric type rob_index,
                      numeric type prf_index );
  `ifdef iclass
    method ActionValue#(Tuple2#(MSHR_status_resp, Bit#(TLog#(mshrsize)))) lookup (Cache_req#(paddr, data, rob_index, prf_index) req);
    method Action allocate (Cache_req#(paddr, data, rob_index, prf_index) req, MSHR_status_resp status, Bit#(TLog#(mshrsize)) mshr_id);
  `else
    method ActionValue#(Tuple2#(MSHR_status_resp, Bit#(TLog#(mshrsize)))) allocate (Cache_req#(paddr, data, rob_index, prf_index) req);
  `endif
    method ActionValue#(Maybe#(MSHR_Req#(paddr, data, prf_index, rob_index))) req_to_fb(Maybe#(Bit#(TLog#(mshrsize))) v_req_rid);
    (*always_ready*) method Bit#(linewidthbits) mem_req_offset(Bit#(TLog#(mshrsize)) id);
    method Bit#(TSub#(paddr,linewidthbits)) addr_to_fb;
    method Action ack_from_fb;
    method Action flush (Flush_type#(rob_index) bundle);
    method Action fence;
    (*always_ready*) method Bool not_empty;
    (*always_ready*) method Bool entries_full;
    (*always_ready*) method Bool one_fifo_full;
    method Action fb_released;
    `ifdef prefetch
      method Origin mshr_primary_request(Bit#(TLog#(mshrsize)) rid);
    `endif
          `ifdef simulate
            `ifdef fesvr_sim
              `ifndef baremetal_sim
                method Action debug_print();
              `endif
            `endif
          `endif
  endinterface

  module mkmshr (Ifc_mshr#(paddr, linewidthbits, data, mshrsize, mshrfifo_depth, rob_index, prf_index))
         provisos ( Add#(addr_in_mshr, linewidthbits, paddr),
                     Add#(mshrfifo_depth, 0, `Mshrfifo_depth),
                    Add#(a__, prf_index, data)
                    );
    let paddr_val= valueOf(paddr);
    let linewidthbits_val= valueOf(linewidthbits);
    let mshrsize_val= valueOf(mshrsize);
    let mshrfifo_depth_val= valueOf(mshrfifo_depth);

    function Bool should_flush(Bit#(rob_index) head, Bit#(rob_index) flush_rob, Bit#(rob_index) rob);
      Bool lv_should_flush= False;
      Bool cond1= (rob>=flush_rob);
      Bool cond2= (rob<head && head!=0);

      if(head<=flush_rob) begin
        if( cond1 || cond2 ) begin
          lv_should_flush= True;
        end
      end
      else if(cond1 && cond2) begin
        lv_should_flush= True;
      end

      return lv_should_flush;
    endfunction

    Reg#(Bit#(addr_in_mshr)) rg_mshr_line_addr [mshrsize_val];
    `ifdef atomic
      Reg#(Tuple2#(Bit#(5), Bit#(prf_index))) rg_atomic_info <- mkConfigReg(tuple2(0,0)); //TODO reset on fence and flush
    `endif
    Reg#(Bool) rg_mshr_valid [mshrsize_val];
    `ifdef prefetch
      Vector#(mshrsize, Reg#(Origin)) rg_mshr_primary_request <- replicateM(mkReg(unpack(0)));
    `endif
    //TODO Does rg_curr_fb_id really need to be Maybe#. Is this correct?
    Reg#(Maybe#(Bit#(TLog#(mshrsize)))) rg_curr_fb_id <- mkConfigReg(tagged Invalid);
    Reg#(Bool) rg_fence <- mkConfigReg(False);
    Reg#(Bool) rg_wait_state <- mkReg(False);
    Reg#(Flush_type#(rob_index)) rg_flush[2] <- mkCReg(2, defaultValue);

    FIFOF#(MSHR_FIFO#(linewidthbits, data)) ff_mshr [mshrsize_val];

    Wire#(Maybe#(Bit#(TLog#(mshrsize)))) wr_curr_req_mshr_id <- mkDWire(tagged Invalid);
    Wire#(Maybe#(Bit#(TLog#(mshrsize)))) wr_allocate_id <- mkDWire(tagged Invalid);
    Wire#(Maybe#(Bit#(TLog#(mshrsize)))) wr_deq_ff_id <- mkDWire(tagged Invalid);
    Wire#(Bit#(addr_in_mshr)) wr_addr_to_fb <- mkDWire(0);
    `ifdef iclass
      Wire#(Bool) wr_fb_released <- mkDWire(False);
    `else
      Reg#(Bool) rg_fb_released <- mkConfigReg(False);
    `endif

    //Create a structure with unguarded single enq, deq and first; and another initialize method which updates
    //all the entries. Can enqueue be stalled for a cycle? Will any deadlock happen if stalled? Will
    //any false response be sent to the processor? If it cannot be stalled, how to combine the data of
    //enq and initialize method?
    //Updating cff_valid at one shot would work as it would reset the valid bit to 0 if should_flush
    //function returns True, and otherwise leave the entry unchanged. Also, whenever any corresponding
    //ff_mshr is enqueued a 1 is enqueued inside, and when ff_mshr is dequeued, cff_valid is also dequeued.
    Ifc_SESFMI_FIFO#(mshrfifo_depth, Bit#(1)) cff_valid [mshrsize_val];
    Ifc_SEMF_FIFO#(mshrfifo_depth, Tuple2#(Bit#(rob_index), Bool)) cff_rob [mshrsize_val];
    for(Integer i=0; i< mshrsize_val; i=i+1) begin
      cff_valid[i] <- mkSESFMI_mshr_inst;
      cff_rob[i] <- mkSEMF_FIFO(?);
    end

    Bool one_mshr_fifo_full= False;
    Bool mshr_full= True;
    Bool mshr_not_empty= False;
                `ifdef prefetch_throttle
                  Bool mshr_almost_full = False;
                  Integer mshr_valid_count = 0;
                `endif

    for(Integer i=0; i<mshrsize_val; i=i+1) begin
      rg_mshr_line_addr[i] <- mkConfigReg(0);
      rg_mshr_valid[i] <- mkConfigReg(False);
      ff_mshr[i] <- mkGSizedFIFOF(True, True, mshrfifo_depth_val);  //TODO check if both enq and deq should be unguarded
      one_mshr_fifo_full= one_mshr_fifo_full || !ff_mshr[i].notFull;
      mshr_full= mshr_full && rg_mshr_valid[i];
      mshr_not_empty= mshr_not_empty || rg_mshr_valid[i];
                  `ifdef prefetch_throttle
                        mshr_valid_count = rg_mshr_valid[i] ? mshr_valid_count + 1 : mshr_valid_count;
                  `endif
    end
                `ifdef prefetch_throttle
                  if ((mshrsize_val-mshr_valid_count) <= 2) begin
                    mshr_almost_full = True;
                  end
                `endif

    rule rl_update_mshr_valid;
      if(wr_allocate_id matches tagged Valid .allocate_id) begin
        `logTimeLevel( dcache, 1, $format("MSHR: setting mshr[%d] valid", allocate_id))
        rg_mshr_valid[allocate_id]<= True;
      end

      if(rg_curr_fb_id matches tagged Valid .curr_fb_id &&&  !ff_mshr[curr_fb_id].notEmpty) begin
        if(wr_allocate_id matches tagged Valid .allocate_id &&& allocate_id== curr_fb_id) begin
          `ifdef iclass
          `logTimeLevel( dcache, 1, $format("MSHR: ff_mshr[%d] is empty, but new allocation to the same MSHR in this cycle. fb_released %d ", curr_fb_id, wr_fb_released))
          `else
          `logTimeLevel( dcache, 1, $format("MSHR: ff_mshr[%d] is empty, but new allocation to the same MSHR in this cycle. fb_released %d ", curr_fb_id, rg_fb_released))
          `endif
        end
        `ifdef iclass
        else if(wr_fb_released) begin
        `else
        else if(rg_fb_released) begin
        `endif
          `logTimeLevel( dcache, 1, $format("MSHR: rg_mshr_valid[%d] is assigned False", curr_fb_id))
          rg_mshr_valid[curr_fb_id]<= False;
        end
        else begin
          `logTimeLevel( dcache, 1, $format("MSHR: MSHR[%d] is empty, but not yet released. Waiting for FB to release.", curr_fb_id))
        end
      end
      //else begin
      //  `logTimeLevel( dcache, 1, $format("MSHR: rg_curr_fb_id: ", fshow(rg_curr_fb_id)))
      //end
    endrule

    rule rl_deq_ff(wr_deq_ff_id matches tagged Valid .deq_ff_id);
      let id= deq_ff_id;
      let lv_req_prf= ff_mshr[id].first.payload;
      `logTimeLevel( dcache, 1, $format("MSHR[%d]: Flushed request for prf_index: %d being dequeued from FIFOs", id, lv_req_prf))
      ff_mshr[id].deq;
      cff_rob[id].deq;
      cff_valid[id].deq;
    endrule

    //TODO This wait state can be removed?
    rule rl_done_fencing(!rg_wait_state && rg_fence && !mshr_not_empty);
      rg_wait_state<= True;
    endrule

    rule rl_reset_fence(rg_wait_state && !mshr_not_empty && rg_fence);
      rg_fence<= False;
      rg_wait_state<= False;
    endrule

`ifdef iclass
    // separate lookup and allocate
    method ActionValue#(Tuple2#(MSHR_status_resp, Bit#(TLog#(mshrsize)))) lookup (Cache_req#(paddr, data, rob_index, prf_index) req);
      Bool mshr_allocated = False, mshr_unallocated = False, lv_dropped = False;
      Bit#(TLog#(mshrsize)) mshr_allocated_id = 0, mshr_unallocated_id = 0;
      Bit#(addr_in_mshr) req_line_addr = req.addr[paddr_val-1:linewidthbits_val];
      Tuple2#(MSHR_status_resp, Bit#(TLog#(mshrsize))) lv_status = tuple2(Busy, 0);

      `logTimeLevel( dcache, 1, $format("MSHR : Lookup for line_addr: %h req_addr: %h", req_line_addr, req.addr))

      for(Integer i=0; i<mshrsize_val; i=i+1) begin
        `logTimeLevel( dcache, 1, $format("MSHR[%d]: Valid: %b line_addr: %h", i, rg_mshr_valid[i], rg_mshr_line_addr[i]))
        // 1. initially, set allocated to True if secondary request
        if (rg_mshr_valid[i] && (req_line_addr == rg_mshr_line_addr[i])) begin
          mshr_allocated = True;
          mshr_allocated_id = fromInteger(i);
        end
        // 2. unallocated is True if primary entry is free
        else if (!rg_mshr_valid[i]) begin
          mshr_unallocated = True;
          mshr_unallocated_id = fromInteger(i);
        end
      end // for
      // 3. allocated is True only if secondary entry is free
      if ((mshr_allocated == True) && !ff_mshr[mshr_allocated_id].notFull) begin
        mshr_allocated = False;
        mshr_unallocated = False;
      end

      // special cases for prefetch requests
      `ifdef prefetch_throttle
        if (req.origin == Store_buffer) begin
          // drop prefetch request if secondary miss or MSHR almost full or primary/secondary full
          if (mshr_allocated 
              || (mshr_unallocated && mshr_almost_full)
              || (!mshr_allocated && !mshr_unallocated)) begin
                `logTimeLevel( dcache, 1, $format("MSHR : Prefetch request dropped (secondary/almost-full/full)."))
                lv_status = tuple2(Dropped, 0);
                lv_dropped = True;
          end
        end // Prefetch
      `endif // throttle

      if (!lv_dropped) begin
        // not allocated or unallocated => full (primary/secondary)
        if (!mshr_allocated && !mshr_unallocated) begin
          `logTimeLevel( dcache, 1, $format("MSHR : MSHR full (primary/secondary), no allocation."))
           lv_status = tuple2(Busy, 0);
        end // full

        // secondary request
        else if (mshr_allocated) begin
          `logTimeLevel( dcache, 1, $format("MSHR : Secondary request, allocated MSHR id: %d for addr: %h", mshr_allocated_id, req.addr))
          lv_status = tuple2(Allocated, mshr_allocated_id);
        end // allocated

        // primary request
        else begin // mshr_unallocated
          `logTimeLevel( dcache, 1, $format("MSHR : Primary request, allocated MSHR id: %d for addr: %h", mshr_unallocated_id, req.addr))
          lv_status = tuple2(Not_allocated, mshr_unallocated_id);
        end // unallocated
      end // !dropped

      return lv_status;
    endmethod // lookup

    // allocation of primary/secondary entry corresponding to mshr_id
    // NOTE: status can only be Allocated or Not_allocated (rest handled in nb_dcache)
    method Action allocate (Cache_req#(paddr, data, rob_index, prf_index) req, MSHR_status_resp status, Bit#(TLog#(mshrsize)) mshr_id);
      Bit#(addr_in_mshr) req_line_addr= req.addr[paddr_val-1:linewidthbits_val];
      Bool can_flush= (req.origin!=Store_commit && req.origin!=PTW);

      // TODO: add assertion
      if ((status != Allocated) && (status != Not_allocated)) begin
        `logTimeLevel( dcache, 1, $format("MSHR : Error! Allocate: Status has to Allocated or Not_Allocated."))
        $finish(0);
        $finish(0);
      end

      // primary or secondary request
      if (status == Not_allocated) begin
        `logTimeLevel( dcache, 1, $format("MSHR : Primary allocation in MSHR[%d] for line_addr: %h req_addr: %h", mshr_id, req_line_addr, req.addr))
      end
      else begin
        `logTimeLevel( dcache, 1, $format("MSHR : Secondary allocation in MSHR[%d] for line_addr: %h req_addr: %h", mshr_id, req_line_addr, req.addr))
      end

      rg_mshr_line_addr[mshr_id]<= req_line_addr;
      `ifdef atomic
         if(req.is_atomic)
           rg_atomic_info<= tuple2(req.atomic_fn, req.prf_index);
      `endif
      wr_allocate_id<= tagged Valid mshr_id;

      ff_mshr[mshr_id].enq( MSHR_FIFO{ addr: req.addr[linewidthbits_val-1:0],
                                       access_size: req.access_size,
                                       dest_type: req.dest_type,
                                       payload: req.payload,
                                       origin: req.origin
                                       `ifdef atomic 
                                         , is_atomic: req.is_atomic
                                       `endif });
      cff_rob[mshr_id].enq(tuple2(req.rob, can_flush));
      cff_valid[mshr_id].enq(1'b1);

      `ifdef prefetch
        // mark original primary request type (need this separately as flushed requests are also marked as "prefetch")
        if (status == Not_allocated) begin
          rg_mshr_primary_request [mshr_id] <= req.origin;
        end
      `endif
    endmethod // allocate

`else // !iclass
    //TODO can be optimized by changing one_mshr_fifo_full to checking only if the fifo corresponding 
    //to the mshr that is being allocated is full
    method ActionValue#(Tuple2#(MSHR_status_resp, Bit#(TLog#(mshrsize)))) allocate (Cache_req#(paddr, data, rob_index, prf_index) req);
                        //if(!one_mshr_fifo_full && !mshr_full);
      Bool mshr_allocated= False;
      Bit#(TLog#(mshrsize)) mshr_allocated_id= 0;
      Bit#(TLog#(mshrsize)) mshr_unallocated_id= 0;
      Bit#(addr_in_mshr) req_line_addr= req.addr[paddr_val-1:linewidthbits_val];
      `logTimeLevel( dcache, 1, $format("MSHR : New req for line_addr: %h req_addr: %h", req_line_addr, req.addr))
      for(Integer i=0; i<mshrsize_val; i=i+1) begin
        `logTimeLevel( dcache, 1, $format("MSHR[%d]: Valid: %b line_addr: %h", i, rg_mshr_valid[i], rg_mshr_line_addr[i]))
        if(rg_mshr_valid[i] && (req_line_addr == rg_mshr_line_addr[i])) begin
          mshr_allocated= True;
          mshr_allocated_id= fromInteger(i);
        end
        else if(!rg_mshr_valid[i]) begin  //If an MSHR entry is not allocated
          mshr_unallocated_id= fromInteger(i);
        end
      end
      //if(mshr_allocated)
      //  wr_curr_req_mshr_id<= tagged Valid mshr_allocated_id;

      //If a new request for which MSHR has not been allocated, and MSHR entries are not full,
      if(!mshr_allocated) begin
        if(!mshr_full) begin
        `ifdef prefetch_throttle
          if (!((req.origin == Store_buffer) && mshr_almost_full)) begin
        `endif

          rg_mshr_line_addr[mshr_unallocated_id]<= req_line_addr;
          `ifdef atomic
          if(req.is_atomic)
            rg_atomic_info<= tuple2(req.atomic_fn, req.prf_index);
          `endif
          wr_allocate_id<= tagged Valid mshr_unallocated_id;
          ff_mshr[mshr_unallocated_id].enq(MSHR_FIFO{ addr: req.addr[linewidthbits_val-1:0],
          access_size: req.access_size,
          dest_type: req.dest_type,
          payload: req.payload,
          origin: req.origin
                                                      `ifdef atomic 
                                                        , is_atomic: req.is_atomic
                                                      `endif });
          Bool can_flush= (req.origin!=Store_commit && req.origin!=PTW);
          cff_rob[mshr_unallocated_id].enq(tuple2(req.rob, can_flush));
          cff_valid[mshr_unallocated_id].enq(1'b1);
          `logTimeLevel( dcache, 1, $format("MSHR : New Allocated MSHR id: %d for addr: %h", mshr_unallocated_id, req.addr))
          return tuple2(Not_allocated, mshr_unallocated_id);
        `ifdef prefetch_throttle
          end
          else begin
            // drop prefetch request when MSHR is almost full
      `logTimeLevel( dcache, 1, $format("MSHR : Prefetch request dropped (MSHR almost full)."))
      return tuple2(Dropped, 0);
          end // prefetch req
        `endif
        end // !mshr_full
        else begin
    `logTimeLevel( dcache, 1, $format("MSHR : New MSHR not allocated as MSHRs are full. ", mshr_allocated_id))
          `ifndef prefetch_throttle
              return tuple2(Busy, ?);
          `else
            // drop prefetch request when MSHR is full
            if (req.origin == Store_buffer) begin
        `logTimeLevel( dcache, 1, $format("MSHR : Prefetch request dropped (MSHR full)."))
              return tuple2(Dropped, 0);
            end
            else begin
              return tuple2(Busy, ?);
            end
          `endif
        end // mshr_full
      end // not allocated

      //Else req is to an already allocated MSHR; so, check if the corresponding fifos are notFull
      else if(ff_mshr[mshr_allocated_id].notFull) begin
      `ifdef prefetch_throttle
        if (req.origin != Store_buffer) begin
      `endif
        `ifdef atomic
        if(req.is_atomic)
          rg_atomic_info<= tuple2(req.atomic_fn, req.prf_index);
        `endif
        wr_allocate_id<= tagged Valid mshr_allocated_id;
        ff_mshr[mshr_allocated_id].enq(MSHR_FIFO{ addr: req.addr[linewidthbits_val-1:0],
        access_size: req.access_size,
        dest_type: req.dest_type,
              payload: req.payload,
        origin: req.origin
                                                  `ifdef atomic 
                                                    , is_atomic: req.is_atomic
                                                  `endif });
        Bool can_flush= (req.origin!=Store_commit && req.origin!=PTW);
        cff_rob[mshr_allocated_id].enq(tuple2(req.rob, can_flush));
        cff_valid[mshr_allocated_id].enq(1'b1);
        `logTimeLevel( dcache, 1, $format("MSHR : Allocated MSHR id: %d for addr: %h", mshr_allocated_id, req.addr))
        return tuple2(Allocated, ?);
      `ifdef prefetch_throttle
        end
        else begin
          // drop prefetch request if it is a secondary miss (fifo not full)
    `logTimeLevel( dcache, 1, $format("MSHR : Prefetch request dropped (secondary miss, fifo not full)."))
          return tuple2(Dropped, 0);
        end // prefetch req
      `endif
      end // fifo not full

      else begin
  `logTimeLevel( dcache, 1, $format("MSHR : Already allocated index %d but ff_mshr is full. ", mshr_allocated_id))
        `ifndef prefetch_throttle
            return tuple2(Busy, ?);
        `else
          // drop prefetch request if it is a secondary miss (fifo full)
          if (req.origin == Store_buffer) begin
      `logTimeLevel( dcache, 1, $format("MSHR : Prefetch request dropped (secondary miss, fifo full)."))
            return tuple2(Dropped, 0);
          end
          else begin
            return tuple2(Busy, ?);
          end
        `endif
      end // fifo full
    endmethod
`endif // !iclass

    //TODO make the FIFO guarded and put explicit conditions wherever requried
    //Check if the condition for the method to fire should be mshr_not_empty or that 
    //For whatever MSHR the response has come, that FIFO is not empty.
    method ActionValue#(Maybe#(MSHR_Req#(paddr, data, prf_index, rob_index))) req_to_fb(Maybe#(Bit#(TLog#(mshrsize))) v_req_rid);
      Maybe#(MSHR_Req#(paddr, data, prf_index, rob_index)) req= tagged Invalid;
      Bit#(addr_in_mshr) lv_fb_addr= 'd0;
      `logTimeLevel( dcache, 1, $format("MSHR : rg_curr_fb_id: ", fshow(rg_curr_fb_id)))
      `logTimeLevel( dcache, 1, $format("MSHR : v_req_rid: ", fshow(v_req_rid)))
      if(rg_curr_fb_id matches tagged Invalid &&& v_req_rid matches tagged Valid .req_rid) begin
        if(rg_mshr_valid[req_rid]) begin
          rg_curr_fb_id<= tagged Valid req_rid;
          let fifo_top= ff_mshr[req_rid].first;
          let cfifo_valid= cff_valid[req_rid].first;
          lv_fb_addr= rg_mshr_line_addr[req_rid];
          Bit#(prf_index) prf_id= `ifdef atomic fifo_top.is_atomic? tpl_2(rg_atomic_info): `endif truncate(fifo_top.payload);

          //If a store_commit is pending, perform it irrespective of whether the cfifo_valid bit is 
          //set, or if it is a fence instruction as this store got committed before the flush or fence 
          //operation. Also, the req is valid if cfifo_valid is set and no fence operation is being done.
                                        // Only loads and prefetch requests can be dropped (050321)
          if((fifo_top.origin==Store_commit) || (fifo_top.origin==PTW) || (cfifo_valid==1'b1 && !rg_fence)) begin
            req= tagged Valid MSHR_Req {  addr: {rg_mshr_line_addr[req_rid], fifo_top.addr},
                    access_size: fifo_top.access_size,
                    dest_type: fifo_top.dest_type,
                    payload: fifo_top.payload,
                    origin: fifo_top.origin,
                                          prf_index: prf_id,
                                          rob: tpl_1(cff_rob[req_rid].first)
                                          `ifdef atomic
                                          , is_atomic: fifo_top.is_atomic
                                          , atomic_fn: tpl_1(rg_atomic_info) 
                                          `endif };
            `logTimeLevel( dcache, 1, $format("MSHR : Miss req to FB when rg_curr_fb_id is Invalid: ", fshow(req)))
          end
          else begin
            if(ff_mshr[req_rid].notEmpty) begin //If a flushed req exists, change origin to Store_buffer so that FB doesn't get released, and no response is sent to the core
              req= tagged Valid MSHR_Req {  addr: {rg_mshr_line_addr[req_rid], fifo_top.addr},
                      access_size: fifo_top.access_size,
                      dest_type: fifo_top.dest_type,
                      payload: fifo_top.payload,
                      origin: Store_buffer,
                                            prf_index: prf_id,
                                            rob: tpl_1(cff_rob[req_rid].first)
                                            `ifdef atomic
                                            , is_atomic: fifo_top.is_atomic
                                            , atomic_fn: tpl_1(rg_atomic_info) 
                                            `endif };
            end
            wr_deq_ff_id<= tagged Valid req_rid;  //Deq req as this is a flushed req
          end
        end
        //Else no pending req of current MSHR are pending, hence wait for the fill buffer to get filled
        else begin
          `logTimeLevel( dcache, 1, $format("MSHR : Waiting for fill buffer to get filled for id: ", fshow(v_req_rid)))
        end
      end
      else if(rg_curr_fb_id matches tagged Valid .curr_rid) begin    //The current MSHR's (that is being serviced) id
        `ifdef iclass
        if(wr_fb_released) begin
        `else
        if(rg_fb_released) begin
        `endif
          rg_curr_fb_id<= tagged Invalid;
          `ifndef iclass
          rg_fb_released<= False;
          `endif
          //`ifdef ASSERT
          //  dynamicAssert(!ff_mshr[curr_rid].notEmpty,"ff_mshr[curr_rid] is not Empty when FB is being released");
          //`endif
          `logTimeLevel( dcache, 1, $format("MSHR : No more pending requests of id: %d", curr_rid))
        end
        if(ff_mshr[curr_rid].notEmpty) begin
          let fifo_top= ff_mshr[curr_rid].first;
          let cfifo_valid= cff_valid[curr_rid].first;
          lv_fb_addr= rg_mshr_line_addr[curr_rid];
          Bit#(prf_index) prf_id= `ifdef atomic fifo_top.is_atomic? tpl_2(rg_atomic_info): `endif truncate(fifo_top.payload);

                                        // Only loads and prefetch requests can be dropped (050321)
          if((fifo_top.origin==Store_commit) || (fifo_top.origin==PTW) || (cfifo_valid==1'b1 && !rg_fence)) begin
            req= tagged Valid MSHR_Req {  addr: {rg_mshr_line_addr[curr_rid], fifo_top.addr},
                    access_size: fifo_top.access_size,
                    dest_type: fifo_top.dest_type,
                    payload: fifo_top.payload,
                    origin: fifo_top.origin,
                                          prf_index: prf_id,
                                          rob: tpl_1(cff_rob[curr_rid].first)
                                          `ifdef atomic
                                          , is_atomic: fifo_top.is_atomic
                                          , atomic_fn: tpl_1(rg_atomic_info)
                                          `endif };
            `logTimeLevel( dcache, 1, $format("MSHR : Miss req from MSHR[%d] to FB: ", curr_rid, fshow(req)))
          end
          else begin
            if(ff_mshr[curr_rid].notEmpty) begin //If a flushed req exists, change origin to Store_buffer so that FB doesn't get released, and no response is sent to the core
              req= tagged Valid MSHR_Req {  addr: {rg_mshr_line_addr[curr_rid], fifo_top.addr},
                      access_size: fifo_top.access_size,
                      dest_type: fifo_top.dest_type,
                          payload: fifo_top.payload,
                      origin: Store_buffer,
                                            prf_index: prf_id,
                                            rob: tpl_1(cff_rob[curr_rid].first)
                                            `ifdef atomic
                                            , is_atomic: fifo_top.is_atomic
                                            , atomic_fn: tpl_1(rg_atomic_info)
                                            `endif };
            end
            wr_deq_ff_id<= tagged Valid curr_rid;
          end
        end

      `ifndef iclass // TODO: check
        //curr_id is being serviced right now, but ff_mshr[curr_rid] is empty, then check if wr_allocate_id
        //is to curr_id or not. If so, then rg_curr_fb_id should remain unchanged, else, Invalidate it
        //so that in the next cycle it will be assigned the value
        else if(wr_allocate_id matches tagged Valid .curr_req_rid &&& curr_req_rid==curr_rid) begin //implicit && !ff_mshr[curr_rid].notEmpty
          `logTimeLevel( dcache, 1, $format("MSHR : New req from ff_second_stage to existing MSHR of id: %d", curr_rid))
        end
      `endif // !iclass
      end
      wr_addr_to_fb<= lv_fb_addr; //truncateLSB(tpl_2(req).addr);
      return req;
    endmethod

    method Bit#(linewidthbits) mem_req_offset(Bit#(TLog#(mshrsize)) id);
      return ff_mshr[id].first.addr;
    endmethod

    method Bit#(addr_in_mshr) addr_to_fb;
      return wr_addr_to_fb;
    endmethod

    method Action ack_from_fb if(mshr_not_empty && !isValid(wr_deq_ff_id));
      if(rg_curr_fb_id matches tagged Valid .fb_id &&& ff_mshr[fb_id].notEmpty) begin
        `logTimeLevel( dcache, 1, $format("MSHR : ack from fb for id: %d", fb_id))
        ff_mshr[fb_id].deq;
        cff_rob[fb_id].deq;
        cff_valid[fb_id].deq;
      end
      else begin
      `ifdef ASSERT
        dynamicAssert(True,"MSHR : Ack from fb called when ff_mshr empty");
      `endif
      end
    endmethod

    method Action flush (Flush_type#(rob_index) bundle);
      rg_flush[0]<= bundle;
      if (`VERBOSITY > 1) begin
        `logTimeLevel( dcache, 1, $format("MSHR : Flush: ", fshow(bundle)))
      end
      `logTimeLevel( dcache, 1, $format("MSHR : Flush initiated: ", fshow(bundle)))
      for(Integer i=0; i<mshrsize_val; i=i+1) begin
        Vector#(mshrfifo_depth,Bit#(1)) valid= cff_valid[i].contents;
        Vector#(mshrfifo_depth,Tuple2#(Bit#(rob_index), Bool)) cff_rob_id= cff_rob[i].contents;

        for(Integer j=0; j<mshrfifo_depth_val; j=j+1) begin
          `logTimeLevel( dcache, 1, $format("MSHR : Flush: Initial V[%d][%d]= %b", i,j, valid[j]))
          `logTimeLevel( dcache, 1, $format("MSHR : Flush: Initial Meta[%d][%d]= ", i,j, fshow(cff_rob_id[j])))
          if(should_flush(bundle.head, bundle.flush_rob, tpl_1(cff_rob_id[j])) && tpl_2(cff_rob_id[j])) begin
            valid[j]=0;
            `logTimeLevel( dcache, 1, $format("MSHR : Flush: Invalidating (%d,%d)", i, j))
          end
        end
        `logTimeLevel( dcache, 1, $format("MSHR : Flush: Setting V[%d]= %b", i, valid))
        cff_valid[i].initialize(valid);
      end
    endmethod

    method Action fence if(!rg_fence);
      rg_fence<= True;
    endmethod

    method Bool not_empty;
      return mshr_not_empty;
    endmethod

    method Bool entries_full;
      return mshr_full;
    endmethod

    method Bool one_fifo_full;
      return one_mshr_fifo_full;
    endmethod

    `ifdef prefetch
      method Origin mshr_primary_request(Bit#(TLog#(mshrsize)) rid);
        return rg_mshr_primary_request[rid];
      endmethod
    `endif

  `ifdef iclass
    // mshr status needs to change in cycle2 of fb release
    method Action fb_released();
      wr_fb_released<= True;
    endmethod

  `else // !iclass
    method Action fb_released if(!rg_fb_released);
      rg_fb_released<= True;
    endmethod
  `endif

  `ifdef simulate
    `ifdef fesvr_sim
      `ifndef baremetal_sim
        method Action debug_print();
    for(Integer i=0; i<mshrsize_val; i=i+1) begin
            $display($time, " PT: DCACHE: MSHR[%d]: valid %b line_addr %h", i, rg_mshr_valid[i], rg_mshr_line_addr[i]);
          end
        endmethod
      `endif
    `endif
  `endif

endmodule

/*
  (*synthesize*)
  module mkmshr_instance (Ifc_mshr#(32, 9, 64, 4, 3, 7, 6));
    let ifc();
    mkmshr _temp(ifc);
    return (ifc);
  endmodule
*/
endpackage
