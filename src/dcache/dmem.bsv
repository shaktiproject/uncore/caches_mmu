/*
see LICENSE.iitm

Author : Neel Gala
Email id : neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package dmem;
  import Vector::*;
  import FIFOF::*;
  import DReg::*;
`ifdef async_rst
import SpecialFIFOs_Modified :: * ;
`else
import SpecialFIFOs :: * ;
`endif  
  import BRAMCore::*;
  import FIFO::*;
  import GetPut::*;
  import Connectable::*;

  import dcache_types::*;
  import io_func::*;
  `include "dcache.defines"
  `include "Logger.bsv"
`ifdef dcache
  `ifdef dcache_2rw
    import dcache2rw :: *;
  `elsif dcache_1r1w
    import dcache1r1w :: *;
  `else
    import dcache1rw :: *;
  `endif
`else
  import null_dcache :: *;
`endif
`ifdef hypervisor
  import fa_dtlb_hypervisor :: * ;
  import common_tlb_types :: * ;
`elsif supervisor
  import fa_dtlb :: * ;
  import common_tlb_types :: * ;
`endif

  interface Ifc_dmem;
      // -------------------- Cache related interfaces ------------//
    interface Put#(DMem_request#(`vaddr, TMul#( `dwords, 8),`desize )) receive_core_req;
    interface Get#(DMem_core_response#(TMul#(`dwords, 8), `desize )) send_core_cache_resp;
    method Maybe#(DMem_core_response#(TMul#(`dwords,8),`desize)) send_core_io_resp;

    interface Get#(DCache_io_req#(`paddr, `dbuswidth)) send_mem_io_req;
    interface Put#(DCache_io_response#(`dbuswidth)) receive_mem_io_resp;

  `ifndef core_clkgate
    (*always_ready,always_enabled*)
  `endif
    method Action ma_curr_priv (Bit#(2) c);
    method Action ma_commit_io(Bit#(`desize) currepoch);
    `ifndef core_clkgate
    (*always_ready*)
  `endif
    method Bool mv_dmem_available;

  `ifdef dcache
    method DCache_mem_writereq#(`paddr, TMul#(`dblocks, TMul#(`dwords, 8))) send_mem_wr_req;
    interface Put#(DCache_mem_writeresp) receive_mem_wr_resp;
    method Action deq_mem_wr_req;

    interface Get#(DCache_mem_readreq#(`paddr)) send_mem_rd_req;
    interface Put#(DCache_mem_readresp#(`dbuswidth)) receive_mem_rd_resp;

    //method Action ma_commit_store(Bit#(`desize ) currepoch);
    method Action ma_commit_store(Tuple2#(Bit#(`desize), Bit#(TLog#(`dsbsize))) storecommit);
    method Action ma_cache_enable(Bool c);
    `ifndef core_clkgate
    (*always_ready*)
  `endif
    method Bool mv_storebuffer_empty;
  `endif
      // ---------------------------------------------------------//
      // - ---------------- TLB interfaces ---------------------- //
  `ifdef supervisor
    interface Get#(DMem_core_response#(TMul#(`dwords, 8), `desize)) get_ptw_resp;
    interface Get#(PTWalk_tlb_request#(`vaddr)) get_req_to_ptw;
    interface Put#(PTWalk_tlb_response#(`ifdef RV64 54, 3 `else 32, 2 `endif )) put_resp_from_ptw;
    /*doc:method: method to receive the current satp csr from the core*/
    method Action ma_satp_from_csr (Bit#(`vaddr) s);

    /*doc:method: method to receive the current values of the mstatus register*/
    method Action ma_mstatus_from_csr (Bit#(`vaddr) m);
    interface Get#(DCache_core_request#(`vaddr, TMul#(`dwords, 8), `desize)) get_hold_req;
  `endif
`ifdef perfmonitors
  `ifdef dcache
    method Bit#(13) mv_dcache_perf_counters;
  `endif
  `ifdef supervisor
    method Bit#(1) mv_dtlb_perf_counters ;
  `endif
`endif
  `ifdef dcache_ecc
    method Maybe#(ECC_dcache_data#(`paddr, `dways, `dblocks)) mv_ded_data;
    method Maybe#(ECC_dcache_data#(`paddr, `dways, `dblocks)) mv_sed_data;
    method Maybe#(ECC_dcache_tag#(`paddr, `dways)) mv_ded_tag;
    method Maybe#(ECC_dcache_tag#(`paddr, `dways)) mv_sed_tag;
    method Action ma_ram_request(DRamAccess access);
    method Bit#(`respwidth) mv_ram_response;
  `endif
  `ifdef hypervisor
   	method Action ma_vsatp_from_csr (Bit#(`vaddr) vsatp);	//For VS-stage translation (if v = 1)
   	method Action ma_vsstatus_from_csr (Bit#(`vaddr) vsstatus);
  `endif
      // ---------------------------------------------------------//
  endinterface

  function DCache_core_request#(`vaddr, TMul#(`dwords,8), `desize ) get_cache_packet
                                    (DMem_request#(`vaddr, TMul#(`dwords, 8), `desize) req);
          return DCache_core_request{ address   : req.address,
                                      fence     : req.fence,
                                      epochs    : req.epochs,
                                      access    : req.access,
                                      size      : req.size,
                                      data      : req.writedata
                                    `ifdef atomic
                                      ,atomic_op : req.atomic_op
                                    `endif
                                    `ifdef supervisor
                                      ,ptwalk_req: req.ptwalk_req
                                    `endif };
  endfunction
`ifdef supervisor
  function DTLB_core_request#(`vaddr) get_tlb_packet
                                    (DMem_request#(`vaddr, TMul#(`dwords, 8), `desize) req);
          return DTLB_core_request{   address   : req.address,
                                      access    : (req.access== 2 && req.atomic_op[3:0]=='b0101) ? 0 : req.access,
                                      cause     : truncate(req.writedata),
                                      ptwalk_trap: req.ptwalk_trap,
                                      ptwalk_req: req.ptwalk_req,
                                      sfence    : req.sfence,
                                      prv       : req.prv
                                    `ifdef hypervisor
                                      , hfence    : req.hfence
                                      , virt      : req.virt
                                      , hlvx      : req.hlvx
                                    `endif
                                      };
  endfunction
`endif

 `ifdef core_clkgate
(*synthesize,gate_all_clocks*)
`else
  (*synthesize*)
`endif
  module mkdmem#(parameter Bit#(32) id
    `ifdef pmp ,
        Vector#(`pmpentries, Bit#(8)) pmp_cfg, 
        Vector#(`pmpentries, Bit#(`paddr)) pmp_addr `endif
        `ifdef testmode ,Bool test_mode `endif )(Ifc_dmem);

    let dcache <- mkdcache(id `ifdef pmp ,pmp_cfg, pmp_addr `endif  `ifdef testmode ,test_mode `endif );
  `ifdef supervisor
    Ifc_fa_dtlb dtlb <- mkfa_dtlb(id);
    mkConnection(dtlb.get_core_response, dcache.put_pa_from_tlb);
  `endif
    interface receive_core_req = interface Put
      method Action put (DMem_request#(`vaddr, TMul#( `dwords, 8),`desize ) r) if (dcache.mv_cache_available);
        `logLevel( dmem, 0, $format("DMEM: Req from Core:",fshow(r)))
      `ifdef supervisor
        if(r.ptwalk_req || (!r.sfence `ifdef hypervisor && !r.hfence `endif ))
            dcache.receive_core_req.put(get_cache_packet(r));
        if(!r.fence)
            dtlb.put_core_request.put(get_tlb_packet(r));
      `else
        dcache.receive_core_req.put(get_cache_packet(r));
      `endif
      endmethod
    endinterface;
    interface send_core_cache_resp = dcache.send_core_cache_resp;
    method send_core_io_resp = dcache.send_core_io_resp;
    interface send_mem_io_req = dcache.send_mem_io_req;
    interface receive_mem_io_resp = dcache.receive_mem_io_resp;
    method ma_cache_enable =  dcache.ma_cache_enable;
`ifdef dcache
    method send_mem_wr_req = dcache.send_mem_wr_req;
    interface receive_mem_wr_resp = dcache.receive_mem_wr_resp;
    interface send_mem_rd_req = dcache.send_mem_rd_req;
    interface receive_mem_rd_resp = dcache.receive_mem_rd_resp;
    method deq_mem_wr_req = dcache.deq_mem_wr_req;
`endif
    method ma_commit_store = dcache.ma_commit_store;
    method ma_commit_io = dcache.ma_commit_io;
    method mv_dmem_available    =dcache.mv_cache_available `ifdef supervisor && dtlb.mv_tlb_available `endif ;
    method mv_storebuffer_empty  =dcache.mv_storebuffer_empty;
    method Action ma_curr_priv (Bit#(2) c);
      dcache.ma_curr_priv(c);
    `ifdef supervisor
      `ifndef hypervisor
	      dtlb.ma_curr_priv(c);
      `endif
      `endif
    endmethod
  `ifdef supervisor
    interface get_ptw_resp = dcache.get_ptw_resp;
    interface get_req_to_ptw = dtlb.get_request_to_ptw;
    interface put_resp_from_ptw = dtlb.put_response_frm_ptw;
    method ma_satp_from_csr = dtlb.ma_satp_from_csr;
    method ma_mstatus_from_csr = dtlb.ma_mstatus_from_csr;
    interface get_hold_req = dcache.get_hold_req;
  `endif
`ifdef perfmonitors
  `ifdef dcache
    method mv_dcache_perf_counters = dcache.mv_perf_counters;
  `endif
  `ifdef supervisor
    method mv_dtlb_perf_counters = dtlb.mv_perf_counters;
  `endif
`endif
  `ifdef dcache_ecc
    method mv_ded_data = dcache.mv_ded_data;
    method mv_sed_data = dcache.mv_sed_data;
    method mv_ded_tag = dcache.mv_ded_tag;
    method mv_sed_tag = dcache.mv_sed_tag;
    method ma_ram_request = dcache.ma_ram_request;
    method mv_ram_response = dcache.mv_ram_response;
  `endif
  `ifdef hypervisor
   	method ma_vsatp_from_csr = dtlb.ma_vsatp_from_csr;
   	method ma_vsstatus_from_csr = dtlb.ma_vsstatus_from_csr;
  `endif
  endmodule
endpackage

